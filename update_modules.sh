#!/usr/bin/env bash

# Update Drupal modules.
#
# If a security update is available for modules, get them and commit to git.
#
# @TODO should abort if git repo is not clean.

# Make the script exit when a command fails.
set -o errexit

# Exit if the script tries to use undeclared variables.
set -o nounset

anyUpdates=false
#   for each module update
for moduleInfo in $(drush pm-updatestatus --security-only --format=csv --fields=name,existing_version,candidate_version); do
  anyUpdates=1
  module=$(echo "$moduleInfo" | cut -d, -f1)
  #     get old version
  previousVer=$(echo "$moduleInfo" | cut -d, -f2)
  #     Download that module
  drush pm-updatecode "$module" --yes
  drush updatedb --yes
  # get new version
  currentVer=$(echo "$moduleInfo" | cut -d, -f3)
  # Get nice name, if available
  niceName=$(drush pm-info --fields=title "$module" --format=list)
  if [[ ! $niceName ]]; then
    niceName=$module
  fi
  # Git commit that module
  # FIXME git add all isn't safe if the git repo wasn't previously clean.
  git add --all
  message="(security) update $niceName module $previousVer => $currentVer"
  echo "$message"
  git commit --message "$message"
done

if [[  "$anyUpdates" = false ]]; then
  echo "No modules to update."
fi
