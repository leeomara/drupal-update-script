#!/usr/bin/env bash

# Update a list of Drupal sites.
#
# Given a list of repo names, place each in a directory, sync from live or stage
# then update core and contrib in a drupal_update branch.

# Make the script exit when a command fails.
set -o errexit

# Exit if the script tries to use undeclared variables.
set -o nounset

repoNames="sportnorth-d7 mining-heritage-society-d7 slpb-website-d7 tusaajiit-d7 cleanconcept_d7 arcan-website-d7 irc-website-d7 polar-bear-swim-club-main-drupal alappaa-d7"
localTLD=".lee"
sitesRoot="$HOME/Sites"

# Find the directory this script is in.
#
# See http://stackoverflow.com/a/246128
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  scriptDir="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
scriptDir="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

# for each git repo
for repoName in $repoNames; do
    dir="$sitesRoot/$repoName$localTLD"

    "$scriptDir"/update_site.sh "$repoName" "$dir"

done
echo ""
echo "All done"
