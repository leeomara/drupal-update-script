#!/usr/bin/env bash

# Update Drupal core.
#
# If a security update is available for Drupal, get it and commit to git.
#
# @TODO should abort if git repo is not clean.

# Make the script exit when a command fails.
set -o errexit

# Exit if the script tries to use undeclared variables.
set -o nounset

drupalVersions=$(drush pm-updatestatus drupal --security-only --fields=existing_version,candidate_version --format=csv)
if [ $drupalVersions ]; then
  #   drush pm-update drupal
  previousVer=$(echo "$drupalVersions" | cut -d, -f1)
  nextVer=$(echo "$drupalVersions" | cut -d, -f2)
  drush pm-update --security-only drupal --yes
  # FIXME git add all isn't safe if the git repo wasn't previously clean.
  git add --all
  message="(security) Drupal update $previousVer => $nextVer"
  echo $message
  git commit --message "$message"
else
  echo "Drupal up to date."
fi
