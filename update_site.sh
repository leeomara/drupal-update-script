#!/usr/bin/env bash

# Update a Drupal site.
#
# Does the checkout and setup, then defers to update_core and update_modules
# scripts.
#
# @TODO should abort if git repo is not clean.

# Make the script exit when a command fails.
set -o errexit

# Exit if the script tries to use undeclared variables.
set -o nounset

repoBase="git@bitbucket.org:kci/"
updatesBranch="drupal-updates"

if [ "$1" != "" ]; then
  repo="$repoBase$1.git"
else
  echo "Must provide repo name as first argument."
  exit 1
fi

if [ "$2" != "" ]; then
  dir=$2
else
  echo "Must provide directory path as second argument."
  exit 1
fi

# Find the directory this script is in.
#
# See http://stackoverflow.com/a/246128
SOURCE="${BASH_SOURCE[0]}"

while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  scriptDir="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
scriptDir="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

echo ""
echo "$repo"
echo "$dir"
if [ ! -d "$dir" ]; then
  #   if directory does not exist: clone repo into directory
  echo "Checkout the code."
  git clone "$repo" "$dir"
  cd "$dir" || exit
else
  #   else: git update that directory
  echo "Update the existing code."
  cd "$dir" || exit
  git fetch
fi
if [[ $(git branch | grep "$updatesBranch") ]]; then
  git checkout $updatesBranch
else
  git checkout -b $updatesBranch
fi

# Find a live (or stage) alias to sql sync from.
drushAlias=$(drush site-alias | grep live | head -n1)
if [[ ! $drushAlias ]]; then
  drushAlias=$(drush site-alias | grep stage | head -n1)
fi
if [[ ! $drushAlias ]]; then
  echo "Could not find an alias."
  exit 1
fi
#   drush site-install blank-empty site
echo "Setup base site."
# Get rid of any old tables that exist from previous installations
drush sql-drop --yes  --db-url=mysql://root@localhost/drupal_update
sudo chmod -R u+w sites/default
# We use tail to remove the leading <?php line
tail -n +2 "$scriptDir"/dev.settings.php >> sites/default/default.settings.php
drush site-install --db-url=mysql://root@localhost/drupal_update --yes
sudo chmod -R u+w sites/default
git checkout -- sites/default/default.settings.php
#   drush sql-sync from live
echo "Clone DB from $drushAlias."
drush sql-sync "$drushAlias" @self --yes
drush sql-sanitize --yes

"$scriptDir"/update_core.sh

"$scriptDir"/update_modules.sh

echo "Finished with $repo"
