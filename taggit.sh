#!/usr/bin/env bash

# Takes any first argument and adds it to the current date.
#
# e.g. taggit -security
# produces a tag with "-security" after the current date.

# Make the script exit when a command fails.
set -o errexit

# Exit if the script tries to use undeclared variables.
set -o nounset

DATE=$(date "+%Y-%m-%d")
TAGNAME="r$DATE${1-}"
git tag "$TAGNAME" -m "Deploy to live $DATE"
echo "tagged as: $TAGNAME"

# Pause to allow the user to confirm if this should be pushed to origin or not.
read -p "Push $TAGNAME to origin? " -n 1 -r
# move to a new line
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
  git push origin "$TAGNAME"
else
  git tag -d "$TAGNAME"
fi
