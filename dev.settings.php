<?php
// Common settings.php config
//
// To use, add this to a site's settings.php
//
//    // Use common local development settings
//    require_once('/Users/leeo/.drush/dev.settings.php');

error_reporting(E_ALL);
ini_set('display_errors', '1');

$conf['error_level'] = '2';

// Anonymous caching.
$conf['cache'] = 0;
// Block caching - disabled.
$conf['block_cache'] = 0;
// Expiration of cached pages - none.
$conf['page_cache_maximum_age'] = 0;
// Aggregate and compress CSS files in Drupal - off.
$conf['preprocess_css'] = 0;
// Aggregate JavaScript files in Drupal - off.
$conf['preprocess_js'] = 0;

// Minimum cache lifetime - none.
$conf['cache_lifetime'] = 0;
// Cached page compression - off.
$conf['page_compression'] = 0;

// Poison the GA setup.
$conf['googleanalytics_account'] = 'UA-XXXXXXXX-Z';

// Don't send module update emails to nobody.
$conf['update_notify_emails'] = array();

// Try to log email to files.
$conf['mail_system'] = array(
  'default-system' => 'DevelMailLog',
);

// Enable theme debuging.
$conf['theme_debug'] = TRUE;
